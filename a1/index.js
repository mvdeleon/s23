// console.log("Hello World");


let getTrainer = {
	name: 'Ash Ketchum',
	age: 10,
	pokemon: ["Pickachu", "Charizard", "Squirtle", "Balbasaur"],
	friends: {
			hoenn: ["May", "Max"],
			kanto: ["Brock", "Misty"],
	},
	talk: function(){
				console.log("Pickachu I choose you!");
			}
		}


// let getTrainer = {};
// getTrainer.name = 'Ash Ketchum';
// getTrainer.age = 10;
// getTrainer.pokemon = ["Pickachu", "Charizard", "Squirtle", "Balbasaur"];
// // getTrainer.friends = {
// // 	hoenn: "May", "Max";
// // 	kanto: "Brock", "Misty";
// }
// getTrainer.talk: function(){
// 		console.log("Pickachu I choose you!");
// 	}
	
// 		console.log(trainer);
// 	// trainer.talk();


// let trainer = {
			
// 			talk: function(){
// 				console.log("Pickachu I choose you!");
// 			}
// 		}

console.log(getTrainer);

console.log("Result from dot notation:");
console.log(getTrainer.name);
console.log("Result from square bracket notation")
console.log(getTrainer["pokemon"]);
getTrainer.talk();


function Pokemon(name,level){

			
			this.name = name;
			this.level = level;
			this.health = level * 2;
			this.attack = level * 1.5;
			this.tackle = function(target){
				console.log(this.name + " tackled " + target.name);
				
				target.health -= this.attack

				console.log(target.name + " health is now reduced to " + target.health);

				if(target.health<=0){
					target.faint()
				}

			}
			this.faint = function(){
				console.log(this.name + " fainted.")
			}

		}

		let jigglypuff = new Pokemon ("jigglypuff", 56);
		console.log(jigglypuff);

		let psyduck = new Pokemon("Psyduck", 74);
		console.log(psyduck); 

		let snorlax = new Pokemon("Snorlax", 16);
		console.log(snorlax); 
	